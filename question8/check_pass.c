#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include "check_pass.h"
/*
Les informations du fichier password sont formattés de la manière suivante: 

uid:taille_mot_de_passe:mot_de_passe // un user par ligne
uid:taille_mot_de_passe:mot_de_passe
uid:taille_mot_de_passe:mot_de_passe
uid:taille_mot_de_passe:mot_de_passe
*/

int check_password(char tab[],char * given_password){
    int i,password_size,find,comp;
    char * real_password;
    i=0;

    /*Recuperation de uid du user*/
    while (tab[i] != '\0'){
        if (tab[i] == ':'){
            tab[i] = '\0';
            find = 1;
            break;
        }
        i++;
    }

    /*Recherche de la ligne correspondante au inforamtion du user*/
    if (atoi(tab) != getuid()) return NOT_GOOD_UID_CODE;
    /*Récuperation de la taille du mot de passe et comparaison avec la taille du mot de passe donné*/
    i += 1;
    tab[i + 1] = '\0';
    password_size = atoi(tab+i);
    if (password_size != strlen(given_password)) return WRONG_PASSWORD;
    /*Comparaison du mot de passe du fichier et du mot de passe saisie*/
    i += 2;
    real_password = tab + i;
    comp = strncmp(given_password, real_password, password_size);
    if (comp == 0){
        printf("CORRECT PASSWORD\n");
        return CORRECT_PASSWORD;
    }
    else{
        return WRONG_PASSWORD;
    }

    
}


int check_pass(char * given_password){
    
    FILE *f;
    char * current_chaine;
    char ch;
    char buffer[50];
    int i,pass_check;

    i = 0;
    if ((f = fopen(PATH, "r")) == NULL) return -1;
    
    while ((ch=fgetc(f)) != EOF){
        if (ch == '\n'){
            buffer[i]='\0';
            i = 0;
            pass_check = check_password(buffer,given_password);
            if (pass_check != NOT_GOOD_UID_CODE){
                return pass_check;
            }
        }
        else{
            buffer[i] = ch;
            i++;
        }
    }
    fclose(f);
    return PASSWORD_NOT_FOUND;
}
