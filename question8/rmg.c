

/* this file belongs to administrateur, change permissions needed */
#include <stdio.h>
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "check_pass.h"

/* 
path = /home/admin/passwd

0- Compiler les fichiers avec de la programmation modulaire
1- Appeller stat sur le fichier
2- Comparer gid user et gid fichier 
3- Si oui, verifier si droit access en ecriture et execution (W_OK && X_OK)
4- Si oui, demander mot de passe
5- Si oui, effacer fichier
*/

int main(int argc, char * argv[]){
    const char * filename;
    filename = argv[1];
    /*Récuperer les informations du fichier*/
    struct stat stats;
    if (stat(filename, &stats) == -1) exit(EXIT_FAILURE);
    /*Comparer avec le gid de l'utilisateur qui a exécuter le fichier avec celui du fichier*/
    int find,j, nb_groups;
    j = 0;
    find = 0;
    gid_t list[10];
    nb_groups = getgroups(10,list);
    while(j < nb_groups){
        if(stats.st_gid == list[j++]) find = 1;
    }
    if (find == 0){
        perror("Permission Error: User and file does not belongs to the same group");
        abort();
    } 
    /*
    if (access(filename, W_OK && X_OK) == -1){
        perror("Access Error: This user has not the Write and Exection permission on this file");
        abort();
    }*/ 
    /*Demander le mot de passe et le sauvegarder*/
    char password[30];
    printf("Saisir votre mot de passe : \n");
    fflush(stdout);
    scanf("%s",password);
    /*Verifier le mot de passe*/
    int verdict;
    char * given_password = password;
    verdict = check_pass(given_password);
    if (verdict == CORRECT_PASSWORD){
        printf("File %s have been removed\n",filename);
        return remove(filename);
    }
    perror("WRONG PASSWORD [process abort]");
    abort();

}

