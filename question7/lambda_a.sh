#!/bin/bash

echo "#############################SCRIPT LAMBDA_A#######################################"
echo
echo "J'affiche le contenu du fichier-a-0 donc je peux le lire..."
more dir_a/fichier-a-0
echo
echo "Je vais essayer d'effacer fichier-a-2 alors que je n'ai pas le droit ..."
rm dir_a/fichier-a-2
echo
echo -n "Je vais crée un nouveau fichier nommée 'fichier-a-3'..."
touch dir_a/fichier-a-3 && echo "Done"
echo -n "Je vais effacer le fichier-a-3 que j'ai crée..."
rm dir_a/fichier-a-3 && echo "Done"
echo "J'affiche le contenu du fichier-c-0 dans dir_c donc je peux le lire..."
more dir_c/fichier-c-0
echo
echo "Test en tant qu'utilisateur LAMBDA_A - DONE"
echo