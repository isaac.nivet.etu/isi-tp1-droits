#!/bin/bash

echo "#############################SCRIPT ADMINISTRATEUR#######################################"
echo 
echo "J'affiche le contenu du fichier-a-0 donc je peux le lire..."
more dir_a/fichier-a-0  
echo
echo "J'affiche le contenu du fichier-b-0 donc je peux le lire..."
more dir_b/fichier-b-0
echo
echo "J'affiche le contenu du fichier-c-0 donc je peux le lire..."
more dir_c/fichier-c-0
echo
echo -n "Je vais modifier fichier-c-1 en ajoutant mon nom utilisateur dans le fichier ... "
echo "ADMINISTRATEUR" >> dir_c/fichier-c-1 echo "Done" 
echo -n "Je vais crée un nouveau fichier nommée 'fichier-c-2' ..."
touch dir_c/fichier-c-2 && "Done"
echo -n "Je vais effacer les fichiers 'dir_c/fichier-c-2', 'dir_b/fichier-b-2' et 'dir_a/fichier-a-2'..."
rm dir_c/fichier-c-2 dir_b/fichier-b-2 dir_a/fichier-a-2 && echo "Done"
echo "Test en tant qu'utilisateur ADMINISTRATEUR - DONE"
echo