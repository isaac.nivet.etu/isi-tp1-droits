#!/bin/bash

echo "#############################SCRIPT LAMBDA_B#######################################"
echo
echo "J'affiche le contenu du fichier-b-0 donc je peux le lire..."
more dir_b/fichier-b-0  
echo  
echo "Je vais essayer d'effacer fichier-b-2 alors que je n'ai pas le droit ..."
rm dir_b/fichier-b-2
echo
echo -n "Je vais crée un nouveau fichier nommée 'fichier-b-3'..."
touch dir_b/fichier-b-3 && echo "Done"
echo -n "Je vais effacer le fichier-b-3 que j'ai crée..."
rm dir_b/fichier-b-3 && echo "Done"
echo "J'affiche le contenu du fichier-c-0 dans dir_c donc je peux le lire..."
more dir_c/fichier-c-0
echo
echo "Test en tant qu'utilisateur LAMBDA_B - DONE"
echo