#!/bin/bash

echo "#############################SCRIPT INIT#######################################"
echo
echo
echo "Creation des différentes fichiers dans les différentes répertoires..."
echo
echo "Saisir le mot de passe de l'utilisateur administeur[password=admin]:"
su administrateur
echo "Je suis le fichier initiale de DIR_A " > dir_a/fichier-a-0    
echo "Je suis le premier fichier ajouté de DIR_A " > dir_a/fichier-a-1   
echo "Je suis le deuxieme fichier ajouté de DIR_A " > dir_a/fichier-a-2   
echo
echo "Je suis le fichier initiale de DIR_B " > dir_b/fichier-b-0     
echo "Je suis le premier fichier ajouté de DIR_B " > dir_b/fichier-b-1   
echo "Je suis le deuxieme fichier ajouté de DIR_B " > dir_b/fichier-b-2   
echo
echo "Je suis le fichier initiale de DIR_C " > dir_c/fichier-c-0   
echo "Je suis le premier fichier ajouté de DIR_C " > dir_c/fichier-c-1  
echo 
echo "Creation des différentes fichiers dans les différentes répertoires - DONE"
echo