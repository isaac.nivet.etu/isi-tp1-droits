# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: FOFANA, Cheick, cheick.fofana2.etu@univ-lille.fr

- Nom, Prénom, email: NIVET,  Isaac,  isaac.nivet.etu@univ-lill.fr 

## Question 1

*cmd[beg]*
sudo -i  [enter] #mode admin
adduser toto [enter] #creation user
adduser toto ubuntu [enter] #affectation toto au groupe ubuntu
*cmd[end]*

Non le processus lancé par toto ne peut pas écrire dans le fichier, car le système utilise le  
premier triplet pour vérifier les droits d'accès, et nous observons que toto(propriétaire du fichier) n'a
pas le droit d'écriture sur ce fichier.

## Question 2

*cmd[beg]*
[user=ubuntu]
su ubuntu
mkdir mydir [enter]
sudo chmod g-x mydir [enter]
cd mydir && touch data.txt [enter]
[user=toto]
su toto
ls -al mydir [enter]
*cmd[end]*

- Le droit d'exécution sur un répertoire autorise l'accès(ou ouverture) de ce répertoire.
- L'utilisateur 'toto' appartenant au groupe 'ubuntu' ne peut pas accéder au répertoire 'mydir'
  car le droit d'accès à ce répertoire a été pour les utilisateurs appartenant au groupe 'ubuntu'.
- [NOT SURE]L'utilisateur 'toto' ne peut pas lister le contenu du répertoire de 'mydir' car il n'y
  tout simplement pas l'accès.

## Question 3

- ids = (1001, 1001, other_groups=[ 1000, 1001])
- Avant l'activation du flag 'set-user-id', 
  le processus n'arrive pas à ouvrir le fichier 'data.txt' car toto n'a pas le d'accès au répertoire contenant data.txt
- Après l'activation du flag 'set-user-id' avec la commande *cmd* sudo chmod u+s a.out *cmd*,
  le processus arrive à ouvrir et à lire le contenu du fichier 'data.txt'.
 
## Question 4

- (EUID,EGID)= (1001, 1001)
- L'administrateur peut utilisé le flag 'set-user-id' pour permettre la modification de '/etc/passwd'. 

## Question 5

- La commande 'chfn' permet de modifier les informations personnels des utilisateurs contenu dans le fichier "/etc/passwd".
- *cmd* ls -al /usr/bin/chfn *cmd* ---> "-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn"
- L'administrateur permet aux utilisateurs utilisé la commande 'chfn' sans pour autant en être le propriétaire en pouvoir modifier
  l'implementation de la commande(car pas d'accès en écriture).

## Question 6

- Les mots de passe sont stockés dans le fichier '/etc/shadow',ils sont hachés avec une méthode représenter par un chiffre en deux
  signe de dollars.
- les mots de passe ne sont pas stockés dans '/etc/passwd' car tout les utilisateurs y ont accès, cependant seul l'administrateur
  a acccès à '/etc/shadow', si un utilisateur essais d'y accéder l'incident sera reporté à l'administrateur.

## Question 7

Il existe 5 scripts dans le répertoire question7.
Les utilisateurs lambda_a, lambda_b et administrateur ont été créés ainsi que les groupes a,b et c.

- Le premier script à lancer(avec sudo) est question7.sh, ce script permet de créer les différentes répertoires et d'attribuer les différents
  groupes aux utilisateurs et droit propriétaire aux répertoires dir_a, dir_b et dir_c .

- Ensuite en tant qu'utilisateur administrateur avec le mot de passe [password=admin], lancé le script init.sh qui
  permet de créer des fichiers dans les répertoires créé plus haut. 

- Ensuite en tant qu'utilisateur lambda_a avec le mot de passe [password=lambda_a], lancé le script lambda_a.sh qui
  permet de lancé les tests pour l'utilisateur lambda_a.

- Ensuite en tant qu'utilisateur lambda_b avec le mot de passe [password=lambda_b], lancé le script lambda_b.sh qui
  permet de lancé les tests pour l'utilisateur lambda_b.

- Ensuite en tant qu'utilisateur administrateur avec le mot de passe [password=admin], lancé le script administrateur.sh 
  qui permet de lancé les tests pour l'utilisateur administrateur.

- Enfin en tant qu'administeur ubuntu, effacer les répertoires crée plus haut avec la commande "sudo rm -rf dir_a dir_b dir_c" 

## Question 8

- les deux premiers commandes à lancer sont:
  * sudo sh question7.sh    -> permet de crée les différents répertoires et d'attribuer les permissions
  * make question8          -> permet de faire la compilation et d'attribuer les permissions 

- en tant qu'utilisateur lambda_a avec le mot de passe [password=lambda_a], lancer la commande suivante :
  *cmd* echo "Je suis le fichier à supprimer" > dir_a/fichier-a-delete *cmd*

- Toujours en tant qu'utilisateur lambda_a, lancer la commande suivante, le mot de passe du user lambda_a sera lambda_a:
  *cmd* ./password dir_a/fichier-a-delete *cmd*

- Enfin en tant qu'administeur ubuntu, lancer la commande "make clean" 


## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








