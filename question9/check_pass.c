#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include "check_pass.h"
/*
Les informations du fichier password sont formattés de la manière suivante: 

uid:taille_mot_de_passe:mot_de_passe // un user par ligne
uid:taille_mot_de_passe:mot_de_passe
uid:taille_mot_de_passe:mot_de_passe
uid:taille_mot_de_passe:mot_de_passe
*/

int check_UID(char tab[], char currentuid[]){
    int i;
    i=0;

    /*Recuperation de uid du user*/
    while (tab[i] != '\0'){
        if (tab[i] == ':'){
            tab[i] = '\0';
            break;
        }
        i++;
    }
    strcpy(currentuid,tab);
    /*Recherche de la ligne correspondante au inforamtion du user*/
    if (atoi(tab) != getuid()) return NOT_GOOD_UID_CODE;
    return i+1;
    }

int check_password(char tab[], int i, char * given_password){
    int j,password_size,find,comp;
    char * real_password;
    j = i;
    /*Récuperation de la taille du mot de passe et comparaison avec la taille du mot de passe donné*/
    tab[j + 1] = '\0';
    password_size = atoi(tab+j);
    if (password_size != strlen(given_password)) return WRONG_PASSWORD;
    /*Comparaison du mot de passe du fichier et du mot de passe saisie*/
    j += 2;
    real_password = tab + j;
    comp = strncmp(given_password, real_password, password_size);
    if (comp == 0){
        printf("CORRECT PASSWORD\n");
        return CORRECT_PASSWORD;
    }
    else{
        return WRONG_PASSWORD;
    }

    
}


int check_pass(char * given_password){
    
    FILE *f;
    char ch;
    char buffer[50];
    char currentUID[10];
    int i, uid, pass_check;

    i = 0;
    if ((f = fopen(PATH, "r")) == NULL) return -1;
    
    while ((ch=fgetc(f)) != EOF){
        if (ch == '\n'){
            buffer[i]='\0';
            i = 0;
            uid = check_UID(buffer,currentUID);
            if (uid != NOT_GOOD_UID_CODE){
                
                return check_password(buffer,uid,given_password);
            }
        }
        else{
            buffer[i] = ch;
            i++;
        }
    }
    fclose(f);
    return PASSWORD_NOT_FOUND;
}
