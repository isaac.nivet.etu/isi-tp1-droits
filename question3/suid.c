#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


int lecture(char * path)
{
        FILE *f;
        char ch;
        f = fopen(path, "r");
        if (f==NULL) return;

        printf("CONTENU : \n");

        while((ch=fgetc(f)) !=  EOF)
                putchar(ch);
        fclose(f);
        return 0;
}



int main(int argc, char *argv[])
{
        printf("\n");

        int i, gidsize;
        gid_t *grouplist;
        gidsize = getgroups(0, NULL);
        grouplist = malloc( gidsize * sizeof( gid_t ) );
        getgroups( gidsize, grouplist );

        printf("ids = (%d, %d, other_groups=[",geteuid(),getegid());
        for (i = 0; i < gidsize -1 ; i++)
                printf(" %d,", (int) grouplist[i]);
        printf(" %d])\n",grouplist[gidsize -1]);


        printf("Tentative Ouverture de 'mydir/data.txt' ... \n\n");
        lecture("mydir/data.txt");

        printf("\n");
        exit(EXIT_SUCCESS);
}


